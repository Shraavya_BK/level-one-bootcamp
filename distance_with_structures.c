//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>

struct press
{
double x,y;
};

double distance(struct press a,struct press b)
{
double dist;
dist=sqrt(((b.x-a.x)*(b.x-a.x))+((b.y-a.y)*(b.y-a.y)));
return dist; 
}
int main()
{
struct press a,b;
printf("Enter the abcissa and ordinate of point 1\n");
scanf("%lf%lf",&a.x,&a.y);
printf("Enter the abcissa and ordinate of point 2\n");
scanf("%lf%lf",&b.x,&b.y);
printf("Distance between point 1 and 2 is %lf",distance(a,b));
return 0;
}
