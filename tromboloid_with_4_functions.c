//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input1()
{
float x;
printf("Enter the h value for h\n");
scanf("%f",&x);
return x;
}

float input2()
{
float y;
printf("Enter the d value for d\n");
scanf("%f",&y);
return y;
}

float input3()
{
float z;
printf("Enter the b value for b\n");
scanf("%f",&z);
return z;
}

float compute(float x,float y,float z)
{
float part1,part2,final;
part1 = x*y*z;
part2 = y/z;
final = 1.0/3.0 * (part1 + part2);
return final;
}

float output(float a)
{
printf("The volume of tromboloid is %f\n",a);
}

int main()
{
float h,d,b,volume,outp;
h=input1();
d=input2();
b=input3();
volume=compute(h,d,b);
outp=output(volume);
return 0;
}
