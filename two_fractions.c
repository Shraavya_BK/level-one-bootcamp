//WAP to find the sum of two fractions.
#include<stdio.h>
#include<math.h>

struct fract
{
    int num,dem;
};
typedef struct fract Fract ;

Fract input()

{
    Fract a;
    printf("Enter the numerator of fraction\n");
    scanf("%d",&a.num);

    printf("Enter the denominator of fraction\n");
    scanf("%d",&a.dem);

    return a;
}
int GCD(int numerator,int denominator)
{
    
int i,gcd;
for(i=2;i<=numerator && i<=denominator;++i)
             {
if(numerator%i==0 && denominator%i==0)
                           {
gcd=i;
        }
 }
    return gcd;
}
Fract compute(Fract a,Fract b)

{
Fract s;
int numerator,denominator,gcd ,i;
numerator=(a.num*b.dem)+(b.num*a.dem);
denominator=(a.dem*b.dem);
gcd=GCD(numerator,denominator);
s.num=numerator/gcd;
s.dem=denominator/gcd;

return s;
}


void output(Fract a,Fract b,Fract s)
     {
printf("the sum of two fractions %d / %d and %d / %d is %d / %d",a.num,a.dem,b.num,b.dem,s.num,s.dem);
     }

int main()
{
Fract num,dem,s;

printf("fraction 1:\n");
num=input();

printf("fraction 2:\n");
dem=input();

s=compute(num,dem);
output(num,dem,s);

return 0;
}


