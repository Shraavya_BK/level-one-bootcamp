//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>

int input1()
{
int x;
printf("Enter a number : \n");
scanf("%d",&x);
return x;
}

int input2()
{
int y;
printf("Enter a number : \n");
scanf("%d",&y);
return y;
}
int sum(int x,int y)
{
int _sum; 
_sum = x + y;
return _sum;
}
int output(int x,int y,int z)
{
printf("The sum of the numbers %d and %d is %d\n",x,y,z);
}
int main()
{
int a,b,c;
a=input1();
b=input2();
c=sum(a,b);
output(a,b,c);
return 0;
}
