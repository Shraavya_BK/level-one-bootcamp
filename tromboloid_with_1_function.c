//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>

int main()
{
float h,d,b,volume,f1,f2;
printf("Enter the value of h\n");
scanf("%f",&h);
printf("Enter the value of d\n");
scanf("%f",&d);
printf("Enter the value of b\n");
scanf("%f",&b);
//i am using f1 for one part of the formula and f2 for the other part to simplify
f1=h*d*b;
f2=d/b;
volume = 1.0/3.0 *(f1+f2);
printf("The volume of given tromboloid is : %f",volume);
return 0;
}
